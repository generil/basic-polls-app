"""
Registration views
"""

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic.base import TemplateView

from .models import User


class SignInView(TemplateView):
    """
    The sign in view
    """

    template_name = 'registration/sign_in.html'

    def get(self, request):
        """
        Displays the sign in page
        """

        if request.user.is_authenticated:
            return redirect('/')
        return self.render_to_response(None)

    def get_context_data(self, username, **kwargs):
        """
        Fetches the data needed for signing in
        """

        context = super(SignInView, self).get_context_data(**kwargs)
        context = {
            'error': 'Invalid username or password',
            'username': username
        }
        return context

    def post(self, request):
        """
        Triggers the signing in
        """

        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return redirect('/')
        else:
            context = self.get_context_data(username)
            return self.render_to_response(context)


def sign_up(request):
    """
    Request for a new user signing up
    """

    if request.user.is_authenticated:
        return redirect('/')

    context = {}
    data = {}

    if request.method == 'POST':
        data['username'] = request.POST.get('username')
        data['first_name'] = request.POST.get('first_name')
        data['last_name'] = request.POST.get('last_name')
        data['email'] = request.POST.get('email')
        data['password'] = request.POST.get('password')

        datacheck = user_integrity_check(data)

        if datacheck == True:
            created = User.objects.create_user(
                username = data['username'],
                first_name = data['first_name'],
                last_name = data['last_name'],
                password = data['password'],
                email = data['email']
            )

            created.save()

            user = authenticate(
                username=data['username'],
                password=data['password']
            )

            login(request, user)
            return redirect('/')
        else:
            context = datacheck[1]

    return render(request, 'registration/sign_up.html', context=context)


def user_integrity_check(data):
    """
    Checks for existing user's information if it's already taken
    """

    errors = {}

    if len(User.objects.filter(username=data.get('username'))) > 0:
        errors['username_error'] = "Username already exists.\
            Do you want to sign in instead?"
        return False, errors

    if len(User.objects.filter(email=data.get('email'))) > 0:
        errors['email_error'] = "Someone has this email already."
        return False, errors

    return True


def sign_out(request):
    """
    Logs out the user
    """

    logout(request)
    return redirect('registration:sign_in')
