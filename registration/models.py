"""
Registration models
"""

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

class UserProfile(models.Model):
    """
    The user's  profile
    """

    user = models.OneToOneField(User, related_name='profile')
    avatar = models.FileField(blank=True)

    def __str__(self):
        return self.user.username

def create_person(sender, instance, created, **kwargs):
    """
    Creates a user
    """

    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)

post_save.connect(create_person, sender=User)
