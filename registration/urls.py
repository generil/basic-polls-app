from django.conf.urls import url
from . import views

app_name = 'registration'
urlpatterns = [
    url(r'^signin/$', views.SignInView.as_view(), name='sign_in'),
    url(r'^signup/$', views.sign_up, name='sign_up'),
    url(r'^signout/$', views.sign_out, name='sign_out'),
]
