from django.contrib import admin
from .models import UserProfile

from django.db.models.signals import post_save

admin.site.register(UserProfile)
