"""
Poll views
"""

from math import ceil
from django.views.generic.base import TemplateView
from django.views import View
from django.urls import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.utils import timezone

from .models import Question, Choice, UserVote


class DetailView(TemplateView):
    """
    Display the area where the user votes for a poll question
    """

    template_name = 'polls/detail.html'

    def get(self, request, pk):
        """
        Displays the detail view
        """

        context = self.get_context_data(request, pk)
        return self.render_to_response(context)

    def get_context_data(self, request, pk, **kwargs):
        """
        Fetches the necessary data to view a poll question
        """

        question = get_object_or_404(Question, pk=pk)
        user_voted = UserVote.objects.filter(
            voter=request.user, voted_question=question
        )
        context = super(DetailView, self).get_context_data(**kwargs)
        if user_voted.exists():
            context['already_voted'] = True
        context['question'] = question
        return context


class ResultView(TemplateView):
    """
    Shows the poll results view
    """

    template_name = 'polls/results.html'

    def get(self, request, pk):
        """
        Displays the results of a vote
        """

        context = self.get_context_data(pk)
        return self.render_to_response(context)

    def get_context_data(self, pk, **kwargs):
        """
        Fetches the necessary data to retrieve the results of a poll question
        """
        context = super(ResultView, self).get_context_data(**kwargs)

        # calculates the percentage of votes
        stats = []
        total_votes = 0
        question = get_object_or_404(Question, pk=pk)
        choices = question.choice_set.all()
        for choice in choices:
            total_votes += choice.votes

        for choice in choices:
            if total_votes == 0:
                stats.append(0)
            else:
                tmp = ceil((choice.votes/total_votes)*100)
                stats.append(tmp)

        zipped = zip(choices, stats)

        context = {
            'total': total_votes,
            'question': question,
            'choices': zipped,
        }

        return context


class VoteView(View):
    """
    Lets you vote on a chosen poll.
    """

    def post(self, request, pk):
        """
        Chooses a choice of the poll question a user inputted
        """

        question = get_object_or_404(Question, pk=pk)
        try:
            choice = question.choice_set.get(pk=request.POST['choice'])
        except(KeyError, Choice.DoesNotExist):
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': 'Select one.'
            })
        else:
            UserVote.objects.create(
                voter=request.user,
                voted_question=question
            )
            choice.votes += 1
            choice.save()
            return HttpResponseRedirect(
                reverse('polls:results', args=(question.id,))
            )
