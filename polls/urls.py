from django.conf.urls import url
from django.views import generic
from . import views

app_name = 'polls'
urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultView.as_view(), name='results'),
    url(r'^(?P<pk>[0-9]+)/vote/$', views.VoteView.as_view(), name='vote'),
]
