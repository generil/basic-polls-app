# Pollr

Pollr is a simple and beautiful polling application.

## FEATURES
* **Create polls.** With Pollr, you can create polls as much as you want.
* **Vote polls.** You are free to vote any polls. To prevent you from voting multiple times, the app restricts you to only one vote.
* **View voting results.** You can see how many votes your bet is having with a beautiful visualization.
* **Create user.** In order for a person to vote. He must have a Pollr account.

[Website](http://generil.pythonanywhere.com)
