"""
Main app views
"""

from django.utils import timezone
from django.views.generic.base import TemplateView
from django.views import View
from django.http import JsonResponse
from django.urls import reverse
from django.contrib.auth.models import User
from django.shortcuts import redirect, get_object_or_404

from polls.models import Question

class IndexView(TemplateView):
    """
    Lists all the polls
    """

    template_name = 'index.html'

    def get(self, request):
        """
        Lists all the poll questions
        """

        if not request.user.is_authenticated:
            return redirect('registration:sign_in')

        context = self.get_context_data()
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        """
        Fetches the data needed for a poll question
        """

        context = super(IndexView, self).get_context_data(**kwargs)
        context['questions'] = Question.objects.all().order_by('-pub_date')
        return context


class ProfileView(TemplateView):
    """
    Shows the user's polls
    """

    template_name = 'profile.html'

    def get(self, request, pk):
        """
        List the user's poll question
        """
        print(pk)
        context = self.get_context_data(pk)
        return self.render_to_response(context)

    def get_context_data(self, pk, **kwargs):
        """
        Fetches all data needed for every poll questions
        """

        context = super(ProfileView, self).get_context_data(**kwargs)
        user = get_object_or_404(User, username=pk)
        question = user.question_set.all().order_by('-pub_date')
        context = {
            'user': user,
            'questions': question
        }

        return context

class AddPollView(View):
    """
    Shows the add poll view
    """

    def post(self, request, *args, **kwargs):
        """
        Adds details of a poll question that a user inputs
        """

        counter = int(request.POST['counter'])
        content = request.POST['question']
        print(counter)
        if counter == 0:
            return JsonResponse({'status': 'failed'})

        question = Question(
            question_text=content,
            pub_date=timezone.now(),
            owner=request.user
        )
        question.save()

        for i in range(0, counter):
            choice = request.POST['choices[' + str(i) + ']']
            question.choice_set.create(choice_text=choice, votes=0)

        return JsonResponse({
            'status': 'success',
            'question': question.question_text,
            'question_results': reverse('polls:results', args=(question.pk,)),
            'question_vote': reverse('polls:detail', args=(question.pk,))
        })