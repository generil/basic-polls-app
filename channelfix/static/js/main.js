requirejs(['jquery', 'mustache', 'less'], function($, Mustache) {
    var counter = 0

    // Adds choices
    $(document).ready(function(){
        $('#add').click(function () {
            if (counter == 0) {
                $(".choices").append(
                    "<input class='form-control' type='text' placeholder='Choice'"
                    + "id='" + counter + "' style='margin-bottom:12px' required> "
                );

                counter += 1;

                $(".add-choice").append(
                    "<button class='btn btn-sm btn-primary' id='go'>Post</button>"
                );
            }
            if(counter < 10) {
                $(".choices").append(
                    "<input class='form-control' type='text' placeholder='Choice'"
                    + "id='" + counter + "' style='margin-bottom:12px' required>"
                );
            }
            if(counter == 9){
                $("#add").remove();
            }
            counter += 1;
        });
    });

    $('#add_question').on('submit', function(e) {
        e.preventDefault();

        var polls_template = "<div class='card bg-light mb-2'>" +
        "<div class='card-body'>" +
        "<h3 class='card-title'>{{question}}</h3>" +
        "<h5 class='float-left'><span class='badge badge-primary'>New</h5>" +
        "<div class='btn-group float-right'>" +
        "<a href='{{question_vote}}' class='btn btn-sm btn-outline-primary'>Vote</a>" +
        "<a href='{{question_results}}' class='btn btn-sm btn-outline-primary'>Results</a>" +
        "</div>" + "</div>" + "</div>"

        var choice = {}

        for(i=0; i<counter; i++) {
            choice[i] = $('#' + i).val();
        }

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: {
                question: $('#poll_name').val(),
                choices: choice,
                counter: counter,
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function(data) {
                if(data.status == 'failed') {
                    alert('Please add some choices.');
                } else {
                    $('#poll_name').val('');
                    $('#polls_list').prepend(Mustache.render(polls_template, data));
                }
            }
        });

        $(".choices").empty();
    });
});