requirejs.config({
    baseUrl: 'js',
    paths: {
        jquery: 'https://code.jquery.com/jquery-3.2.1.min',
        mustache: 'https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min',
        less: 'https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.min'
    }
});